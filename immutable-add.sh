#!/bin/bash

set -e

REDBEAN_PATH="$1"
URL=$2

TMP_PATH=$(mktemp -d -t immweb_XXXXXXXXXX)

wget                       \
    --mirror               \
    --convert-links        \
    --adjust-extension     \
    --page-requisites      \
    --no-parent            \
    --no-if-modified-since \
    --quiet                \
    --directory-prefix=$TMP_PATH \
    $URL

AUX=$(pwd)
cd $TMP_PATH
zip -r $AUX/$REDBEAN_PATH *
